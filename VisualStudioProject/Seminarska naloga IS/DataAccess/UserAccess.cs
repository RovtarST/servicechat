﻿using Seminarska_naloga_IS.Tabele;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Web;

namespace Seminarska_naloga_IS.DataAccess
{
    public class UserAccess
    {
        //static string path1 = Path.GetDirectoryName(Assembly.GetExecutingAssembly().CodeBase).ToString();
        //static string path = path1.Remove(path1.Length-6) + "\\App_Data\\chatdb.mdf";
        //static string path = "C:\\Users\\Simon Tusar\\Source\\Repos\\chatdb\\Seminarska naloga IS\\App_Data\\chatdb.mdf";
        //static string path = "|DataDirectory|\\chatdb.mdf";
        //static string source = "Data Source = (LocalDB)\\MSSQLLocalDB;AttachDbFilename=" + path + ";Integrated Security = True";
        static string source = "Data Source = chatdbbs.database.windows.net; Initial Catalog = CHATDB; Persist Security Info=True;User ID = blazsimon; Password=Hiho1234";

        public static List<User> getUsers()
        {
            DataTable data = new DataTable("User");
            using (SqlConnection connection = new SqlConnection(source))
            {
                connection.Open();
                using (SqlCommand command = new SqlCommand(@"select COUNT(Pogovor.username) as 'stSporocil', Uporabnik.*
                                                             from Uporabnik LEFT JOIN Pogovor ON(Uporabnik.username = Pogovor.username)
                                                             group by Uporabnik.username, Uporabnik.ime, Uporabnik.priimek, Uporabnik.geslo, Uporabnik.admin",
                                                          connection))
                {
                    using (SqlDataAdapter da = new SqlDataAdapter(command))
                        da.Fill(data);
                }
                connection.Close();
            }

            List<User> user_list = new List<User>();

            foreach (DataRow row in data.Rows)
            {
                User user = rowToUporabnik(row);
                user_list.Add(user);
            }

            return user_list;
        }

        public static User GetUporabnikById(int username)
        {
            DataTable data = new DataTable("User");
            using (SqlConnection connection = new SqlConnection(source))
            {
                connection.Open();
                using (SqlCommand command = new SqlCommand("SELECT * FROM Uporabnik Where Uporabnik.username = @username", connection))
                {
                    command.Parameters.Add(new SqlParameter("username", username));
                    using (SqlDataAdapter da = new SqlDataAdapter(command))
                        da.Fill(data);
                }
                connection.Close();
            }


            if (data.Rows.Count == 1)
            {
                User user = rowToUporabnik(data.Rows[0]);

                return user;
            }
            else
            {
                return null;
            }
        }

        public static User rowToUporabnik(DataRow row)
        {
            User user = new User();
            user.setFirstName(row["ime"].ToString());
            user.setLastName(row["priimek"].ToString());
            user.setNumberOfMessages(Int32.Parse(row["stSporocil"].ToString()));
            user.setUserName(row["username"].ToString());
            user.setPassword(row["geslo"].ToString());
            user.setAdmin(row["admin"].ToString());
            return user;
        }
    }
}