﻿using Seminarska_naloga_IS.Tabele;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Web;

namespace Seminarska_naloga_IS.DataAccess
{
    public class MessageAccess
    {
        //private static string path = "C:\\Users\\Simon Tusar\\Source\\Repos\\chatdb\\Seminarska naloga IS\\App_Data\\chatdb.mdf";
        //private static string path = "C:\\Users\\blazr\\Documents\\Šola\\Faks\\2. Letnik\\Informacijski sistemi\\hihane\\Seminarska naloga IS\\App_Data\\chatdb.mdf";
        //static string path1 = Path.GetDirectoryName(Assembly.GetExecutingAssembly().CodeBase).ToString();
        //static string path = path1.Remove(path1.Length - 6) + "\\App_Data\\chatdb.mdf";
        //static string path = "|DataDirectory|\\chatdb.mdf";
        //static string source = "Data Source = (LocalDB)\\MSSQLLocalDB;AttachDbFilename=" + path + ";Integrated Security = True";
        static string source = "Data Source = chatdbbs.database.windows.net; Initial Catalog = CHATDB; Persist Security Info=True;User ID = blazsimon; Password=Hiho1234";

        public static List<string> getMessage()
        {
            List<string> message = new List<string>();
            using (SqlConnection connection = new SqlConnection(source))
            {
                connection.Open();
                using (SqlCommand command = new SqlCommand("SELECT username, besedilo FROM Pogovor", connection))
                {
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            message.Add(reader["username"].ToString() + " : " + reader["besedilo"].ToString());
                        }
                    }
                }
                connection.Close();
            }
            return message;
        }

        public static List<Messages> getMessages(int id)
        {
            List<Messages> message = new List<Messages>();
            using (SqlConnection connection = new SqlConnection(source))
            {
                connection.Open();
                using (SqlCommand command = new SqlCommand("SELECT Id, username, besedilo FROM Pogovor where id > " + id, connection))
                {
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            Messages temp = new Messages();
                            temp.ID = Convert.ToInt32(reader["Id"].ToString());
                            temp.Username = reader["username"].ToString();
                            temp.Text = reader["besedilo"].ToString();
                            message.Add(temp);
                        }
                    }
                }
                connection.Close();
            }
            return message;
        }

        public static bool setMessage(string user, string m)
        {
            bool temp = false;
            using (SqlConnection connection = new SqlConnection(source))
            {
                connection.Open();
                using (SqlCommand command = new SqlCommand("INSERT INTO Pogovor (username, besedilo) VALUES (@username, @message)", connection))
                {
                    command.Parameters.Add(new SqlParameter("username", user));
                    command.Parameters.Add(new SqlParameter("message", m));
                    command.ExecuteNonQuery();
                    temp = true;
                }
                connection.Close();
            }
            return temp;
        }
    }
}