﻿using Seminarska_naloga_IS.DataAccess;
using Seminarska_naloga_IS.Tabele;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace Seminarska_naloga_IS
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Service1" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select Service1.svc or Service1.svc.cs at the Solution Explorer and start debugging.
    public class Service1 : IService1
    {
        private User AuthenticateUser()
        {
            List<User> temp = new List<User>();
            WebOperationContext ctx = WebOperationContext.Current;
            string authHeader = ctx.IncomingRequest.Headers[HttpRequestHeader.Authorization];
            if (authHeader == null)
                return null;

            //var data = Encoding.UTF8.GetString(data).Convert.FromBase64String(authHeader);

            string[] loginData = authHeader.Split('|');
            if (loginData.Length == 2)
            {
                User u = new User().getUserByUsername(loginData[0]);
                if (u != null && u.getPassword().Equals(loginData[1]))
                {
                    return u;
                }
            }
            return null;
        }

        public User Login()
        {
            return AuthenticateUser();
        }

        public bool Send()
        {
            WebOperationContext ctx = WebOperationContext.Current;
            string authHeader = ctx.IncomingRequest.Headers[HttpRequestHeader.Authorization];
            string text = ctx.IncomingRequest.Headers["SporociloText"];
            if (authHeader == null)
                return false;

            string[] loginData = authHeader.Split('|');

            if (AuthenticateUser() != null && text != null)
            {
                return MessageControl.setMessage(loginData[0], text);
            }
            return false;
        }

        public List<Messages> Messages()
        {
            if(AuthenticateUser() != null)
            {
                WebOperationContext ctx = WebOperationContext.Current;
                string authHeader = ctx.IncomingRequest.Headers["Index"];
                
                if (authHeader != null)
                {
                    return MessageAccess.getMessages(Convert.ToInt32(authHeader));
                }
                return MessageAccess.getMessages(-1);
            }
            return null;
        }
    }
}
