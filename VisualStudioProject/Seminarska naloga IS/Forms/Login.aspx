﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="Seminarska_naloga_IS.Login" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Seminarska naloga IS</title>
    <link href="../CSS/Login.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <asp:Label ID="lbTitle" runat="server" Text="ChatDB" CssClass="TitleStyle"></asp:Label>
    </div>
    <div id="divRegistration">
        <p>&nbsp;</p>
        <asp:Label ID="Label6" runat="server" Text="Registracija" CssClass="LoginStyle" Font-Size="Larger"></asp:Label>
        <br />
        <asp:Label ID="Label8" runat="server" Text="Nov uporabnik" CssClass="LoginStyle" Font-Italic="True"></asp:Label>
        <br />
        <br />
        <asp:Label ID="Label1" runat="server" Text="Uporabniško ime" CssClass="LoginStyle"></asp:Label>
        <asp:TextBox ID="tbUsernameIN" runat="server"></asp:TextBox>
        <br />
        <asp:Label ID="Label3" runat="server" Text="Ime" CssClass="LoginStyle"></asp:Label>
        <asp:TextBox ID="tbFirstNameIN" runat="server"></asp:TextBox>
        <br />
        <asp:Label ID="Label4" runat="server" Text="Priimek" CssClass="LoginStyle"></asp:Label>
        <asp:TextBox ID="tbLastNameIN" runat="server"></asp:TextBox>
        <br />
        <asp:Label ID="Label2" runat="server" Text="Geslo" CssClass="LoginStyle"></asp:Label>
        <asp:TextBox ID="tbPasswordIN" runat="server" TextMode="Password"></asp:TextBox>
        <br />
        <asp:Label ID="Label5" runat="server" Text="Geslo" CssClass="LoginStyle"></asp:Label>
        <asp:TextBox ID="tbPasswordIN0" runat="server" TextMode="Password"></asp:TextBox>
        <br />
        <asp:Label ID="lbStatus" runat="server" Text="lbStatus" CssClass="LoginStyle"></asp:Label>
        <br />
        <asp:Button ID="btnInsert" runat="server" Text="insert" OnClick="inserBurron_Click" Font-Bold="False" CssClass="button" Width="199px"/>
    </div>

    <div id="divLabels">
        <br /><br /><br /><br /><br /><br />
        <asp:Label ID="lbUsername" runat="server" Text="Uporabniško ime" CssClass="LoginStyleRight "></asp:Label>
        <br />
        <asp:Label ID="lbPassword" runat="server" Text="Geslo" CssClass="LoginStyleRight "></asp:Label>
    </div>

    <div id="divInput">
        <p>&nbsp;</p>
        <asp:Label ID="Label7" runat="server" Text="Prijava" CssClass="LoginStyle" Font-Size="Larger"></asp:Label>
        <br />
        <asp:Label ID="Label9" runat="server" Text="Obstoječ uporabnik" CssClass="LoginStyle" Font-Italic="True"></asp:Label>
        <br />
        <br />
        <asp:TextBox ID="Username" runat="server" Width="200px"></asp:TextBox>
        <asp:Label ID="lbUsernameError" runat="server" Text="lbUsernameError" Visible="False" CssClass="LoginStyle"></asp:Label>
        <br>
        <asp:TextBox ID="Password" runat="server" TextMode="Password" Width="200px"></asp:TextBox>
        <asp:Label ID="lbPasswordError" runat="server" Text="lbPasswordError" Visible="False" CssClass="LoginStyle"></asp:Label>
        <br />
        <br />
        <asp:Button ID="LoginBtn" runat="server" Text="Prijava" OnClick="LoginBtn_Click" Font-Bold="False" CssClass="button"/>
        <asp:Button ID="btnAdminLogin" runat="server" Text="Prijava v administratorski način" CssClass="button" Width="199px" OnClick="btnAdminLogin_Click"/>
    </div>

    <div style="clear:both"></div>
    </form>
</body>
</html>
