﻿using Seminarska_naloga_IS.Tabele;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Seminarska_naloga_IS.Forms
{
    public partial class Chat : System.Web.UI.Page
    {
        User u = new User();
        List<User> loggedUsers;
        ListBox listUsers = new ListBox();
        ListBox listMessages = new ListBox();
        private readonly string URLSend = "http://seminarskaisbs.azurewebsites.net/Service1.svc/Send";
        private readonly string URLGetMessages = "http://seminarskaisbs.azurewebsites.net/Service1.svc/Messages";
        //private readonly string URLSend = "http://localhost:30116/Service1.svc/Send";
        //private readonly string URLGetMessages = "http://localhost:30116/Service1.svc/Messages";

        protected void Page_Load(object sender, EventArgs e)
        {
            this.Form.DefaultButton = this.Send.UniqueID; //Z entrom lahko pokličemo akcijo, ki je na gumbu Send
            //pridobim sejo uporabnika
            if (!IsPostBack)
            {
                u = (User)Session["user"];                                                  //pridobimo uporabnika, ki se je prijavil
                if(u != null)
                    this.hiddenUsername.Value = u.getUserName();                            //skrijemo username, da ob osvežitvi seje vemo komu pripada
                else
                    Response.Redirect("Login.aspx");
            }
            else
                Session["user"] = new User().getUserByUsername(this.hiddenUsername.Value);  //ko osvežimo sejo, ugotovimo komu pripada

            u = (User)Session["user"];                                                      //pridobimo uporabnika seje

            //v seznam si zapišem uporabnika, ki je trenutno prijavljen
            if (u != null)
            {
                this.CurrentUser.Text = u.ToString();                                       //pozdravimo uporabnika
                loggedUsers = LoggedUsers.getUserList();                                    //pridobimo seznam vseh uporabnikov, ki so prijavljeni
                User u_temp = loggedUsers.Find(x => x.getUserName() == u.getUserName());    
                if (u_temp == null)
                    LoggedUsers.Add(u);
            }

            listUsers.Height = 150;
            listUsers.Width = 160;
            listMessages.Height = 200;
            listMessages.Width = 1200;
            //listMessages.Items.Clear();
            GetData();

            /*
            //izpišem seznam uporabnikov, ki so že prijavljeni            
            //izpišem sporočila
            listMessages.Height = 600;
            listMessages.Width = 600;
           
            string auth = u.getUserName() + "|" + u.getPassword();
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(URLGetMessages);
            client.DefaultRequestHeaders.Add("Authorization", auth);
            if (Session["Index"] != null)
            {
                client.DefaultRequestHeaders.Add("Index", (String)Session["Index"]);
            }
            else
            {
                listMessages.Items.Clear();
            }

            // Add an Accept header for JSON format.
            client.DefaultRequestHeaders.Accept.Add(
            new MediaTypeWithQualityHeaderValue("application/json"));

            // List data response.
            HttpResponseMessage response = client.GetAsync("").Result;  // Blocking call!
            if (response.IsSuccessStatusCode)
            {

                // Parse the response body. Blocking!
                List<Messages> messages = response.Content.ReadAsAsync<List<Messages>>().Result;
                foreach (Messages temp in messages)
                {
                    listMessages.Items.Add(new ListItem(temp.Username + " : " + temp.Text));
                    Session["Index"] = (String)temp.ID.ToString();
                }
                this.Messages.Controls.Add(listMessages);
            }
            ///da ne podvajam sporočil
            List<string> message = MessageControl.getMessage();                             //pridobim vsa že poslana sporočila
            foreach (string m in message)
            {
                listMessages.Items.Add(new ListItem(m));                                    //izpišem vsa poslana sporočila                        
            }
            this.Messages.Controls.Add(listMessages);*/
        }

        protected void GetData()
        {
            loggedUsers = LoggedUsers.getUserList();                                       //kličemo ponovno, ker je lahko prišlo do spremembe
            listUsers.Items.Clear();                                                       //da ne podavajamo uporabnikov
            foreach (User u in loggedUsers)                                                //napolnimo seznam
            {
                listUsers.Items.Add(new ListItem(u.ToString()));
            }
            this.Users.Controls.Add(listUsers);

            //izpišem sporočila
            string auth = u.getUserName() + "|" + u.getPassword();
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(URLGetMessages);
            client.DefaultRequestHeaders.Add("Authorization", auth);
            if (Session["Index"] != null)
            {
                client.DefaultRequestHeaders.Add("Index", (String)Session["Index"]);
                listMessages = (ListBox)Session["listMessages"];
            }

            // Add an Accept header for JSON format.
            client.DefaultRequestHeaders.Accept.Add(
            new MediaTypeWithQualityHeaderValue("application/json"));

            // List data response.
            HttpResponseMessage response = client.GetAsync("").Result;  // Blocking call!
            if (response.IsSuccessStatusCode)
            {
                // Parse the response body. Blocking!
                List<Messages> messages = response.Content.ReadAsAsync<List<Messages>>().Result;
                foreach (Messages temp in messages)
                {
                    listMessages.Items.Add(temp.Username + " : " + temp.Text);
                    Session["Index"] = (String)temp.ID.ToString();
                }
                Session["listMessages"] = (ListBox)listMessages;
                this.Messages.Controls.Add(listMessages);
                //this.ListBoxMessages.DataSource = listMessages;
            }
        }

        protected void LogoutBtn_Click(object sender, EventArgs e)
        {
            //uporabnika odjavimo in izbrišemo iz seznama
            //BUG: ko odjaviš, se ne odstrani iz lista
            //LoggedUsers.Remove(u);
            if(u != null)
                LoggedUsers.deleteByUsername(u.getUserName());
            Response.Redirect("Login.aspx");                                        
        }

        protected void RefreshBtn_Clicked(object sender, EventArgs e)
        {
            //Page_Load(sender, e);                                                          //osvežim stran, da takoj vidimo spremembo
            //GetData();
        }

        protected void SendBtn_Clicked(object sender, EventArgs e)
        {
            if (u != null)
            {
                string auth = u.getUserName() + "|" + u.getPassword();// Convert.ToBase64String(Encoding.UTF8.GetBytes(auth))
                HttpClient client = new HttpClient();
                client.BaseAddress = new Uri(URLSend);
                client.DefaultRequestHeaders.Add("Authorization", auth);
                client.DefaultRequestHeaders.Add("SporociloText", this.Message.Text);

                // Add an Accept header for JSON format.
                client.DefaultRequestHeaders.Accept.Add(
                new MediaTypeWithQualityHeaderValue("application/json"));

                // List data response.
                HttpResponseMessage response = client.GetAsync("").Result;  // Blocking call!
                if (response.IsSuccessStatusCode)
                {
                    // Parse the response body. Blocking!
                    if (!response.Content.ReadAsAsync<bool>().Result)
                    {
                        string script = "alert(\"Message was not send!\");";
                        ScriptManager.RegisterStartupScript(this, GetType(), "ServerControlScript", script, true);
                    }
                    else
                    {
                        this.Message.Text = "";
                        Page_Load(sender, e);
                    }
                }
                /*
                //string message = u.ToString() + ": " + this.Message.Text;                 //pridobim ime uporabnika in njegovo sporočilo
                MessageControl.setMessage(u.getUserName(), this.Message.Text);              //pošljem sporočilo v statični razred
                this.Message.Text = "";                                                     //ne pozabi pobrisati textboxa
                Page_Load(sender, e);       */                                                //osvežim stran, da takoj vidimo spremembo

            }
        }
    }
}