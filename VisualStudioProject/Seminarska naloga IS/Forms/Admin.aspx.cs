﻿using Seminarska_naloga_IS.DataAccess;
using Seminarska_naloga_IS.Tabele;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Seminarska_naloga_IS.Forms
{
    public partial class WebForm1 : System.Web.UI.Page
    {
        User u = new User();
        List<User> listUsers = new List<User>();
        //User selectedUser;

        protected void Page_Load(object sender, EventArgs e)
        {
            this.lbIme.Text         = "";
            this.lbPriimek.Text     = "";
            this.lbStSporocil.Text  = "";
            this.lbUsername.Text    = "";
            this.btnIzbrisi.Enabled = false;
            this.cbJeAdmin.Enabled  = false;
            this.btnPotdriAdministratorja.Enabled = false;

            //pridobim sejo uporabnika
            if (!IsPostBack)
            {
                u = (User)Session["user"];                                                  //pridobimo uporabnika, ki se je prijavil
                if (u != null)
                    this.hiddenUsername.Value = u.getUserName();                            //skrijemo username, da ob osvežitvi seje vemo komu pripada
                //TO popravi
                //else
                //    Response.Redirect("Login.aspx");
            }
            else
                Session["user"] = new User().getUserByUsername(this.hiddenUsername.Value);  //ko osvežimo sejo, ugotovimo komu pripada

            u = (User)Session["user"];                                                      //pridobimo uporabnika seje

            //this.UsersAndMessages.Items.Clear();
            if (this.UsersAndMessages.Items.Count != UserAccess.getUsers().Count)
            {
                foreach (User u in UserAccess.getUsers())                                     //napolnimo seznam
                {
                    this.UsersAndMessages.Items.Add(new ListItem(u.ToString() + " " + u.getNumberOfMessages(), u.getUserName()));
                }
            }
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            Response.Redirect("Login.aspx");
        }


        protected void btnUredi_Click(object sender, EventArgs e)
        {
            String userName = this.UsersAndMessages.SelectedItem.Value.ToString();
            User selectedUser = new User().getUserByUsername(userName);
            this.hiddenSelectedUser.Value = selectedUser.getUserName();

            if (selectedUser != null)
            {
                this.btnIzbrisi.Enabled = true;
                this.cbJeAdmin.Enabled  = true;
                this.btnPotdriAdministratorja.Enabled = true;

                this.lbUsername.Text = selectedUser.getUserName();
                this.lbIme.Text      = selectedUser.getFirstName();
                this.lbPriimek.Text  = selectedUser.getLastName();
                this.lbStSporocil.Text = selectedUser.getNumberOfMessages().ToString();
                if (selectedUser.getAdmin())
                    this.cbJeAdmin.Checked = true;
                else
                    this.cbJeAdmin.Checked = false;
            }
        }

        protected void btnIzbrisi_Click(object sender, EventArgs e)
        {
            User selectedUser = new User().getUserByUsername(this.hiddenSelectedUser.Value);
            if (selectedUser != null)
            {
                selectedUser.Delete();
                this.UsersAndMessages.Items.Clear();
                if (this.UsersAndMessages.Items.Count != UserAccess.getUsers().Count)
                {
                    foreach (User u in UserAccess.getUsers())                                     //napolnimo seznam
                    {
                        this.UsersAndMessages.Items.Add(new ListItem(u.ToString() + " " + u.getNumberOfMessages(), u.getUserName()));
                    }
                }
            }
        }

        protected void btnPotdriAdministratorja_Click(object sender, EventArgs e)
        {
            User selectedUser = new User().getUserByUsername(this.hiddenSelectedUser.Value);
            if (selectedUser != null)
            {
                if (cbJeAdmin.Checked)
                    selectedUser.setAdmin("True");
                else
                    selectedUser.setAdmin("False");
                selectedUser.Update();
            }
        }
    }
}