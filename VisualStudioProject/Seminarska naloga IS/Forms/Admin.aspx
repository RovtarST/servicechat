﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Admin.aspx.cs" Inherits="Seminarska_naloga_IS.Forms.WebForm1" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <asp:ListBox ID="UsersAndMessages" runat="server">
        </asp:ListBox>
        <asp:HiddenField ID="hiddenUsername" runat="server"/>
        <asp:HiddenField ID="hiddenSelectedUser" runat="server"/>
        <br/>
        <asp:Button ID="btnUredi" runat="server" Text="Uredi uporabnika" OnClick="btnUredi_Click" />
        <br/>
        <asp:Button ID="btnIzbrisi" runat="server" Text="Izbrisi uporabnika" OnClick="btnIzbrisi_Click"/>
        <br/>
        <asp:Label ID="Label5" runat="server" Text="Uporabniško ime: "></asp:Label>
        <asp:Label ID="lbUsername" runat="server" Text="lbUsername"></asp:Label>
        <br/>
        <asp:Label ID="Label4" runat="server" Text="Ime: "></asp:Label>
        <asp:Label ID="lbIme" runat="server" Text="lbIme"></asp:Label>
        <br/>
        <asp:Label ID="Label3" runat="server" Text="Priimek: "></asp:Label>
        <asp:Label ID="lbPriimek" runat="server" Text="lbPriimek"></asp:Label>
        <br/>
        <asp:Label ID="Label2" runat="server" Text="Sporočila: "></asp:Label>
        <asp:Label ID="lbStSporocil" runat="server" Text="lbStSporocil"></asp:Label>
        <br/>
       <asp:Label ID="Label1" runat="server" Text="Je administrator: "></asp:Label>
        <asp:CheckBox ID="cbJeAdmin" runat="server"/>
        <asp:Button ID="btnPotdriAdministratorja" runat="server" OnClick="btnPotdriAdministratorja_Click" Text="Potrdi" />
        <br/>
        <asp:Button ID="btnOdjava" runat="server" OnClick="Button1_Click" Text="Odjava" />
    </div>
    </form>
</body>
</html>
