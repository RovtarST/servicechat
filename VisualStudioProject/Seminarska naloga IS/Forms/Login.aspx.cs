﻿using Seminarska_naloga_IS.Forms;
using Seminarska_naloga_IS.Tabele;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Seminarska_naloga_IS
{
    public partial class Login : System.Web.UI.Page
    {
        private User u = new User(); 
        private readonly string URLLogin = "http://seminarskaisbs.azurewebsites.net/Service1.svc/Login";

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Session["user"] = null;
            }
            //če bo kaj narobe tukaj tiči napaka
            else
                u = (User)Session["user"];

            this.lbStatus.Text = "";
            this.lbStatus.Visible = false;
        }

        protected void LoginBtn_Click(object sender, EventArgs e)
        {
            string userName = this.Username.Text;
            string password = this.Password.Text;

            bool isEmpty = false;                                       //v "bazo" gremo preverjati uporabnika šele ko je vse veseno
            Encrtypt en = new Encrtypt();

            //preverim ali so vsa obvezna polja izpolnjena
            if (userName.Equals(""))
            {
                this.lbUsernameError.Text = "Username is empty";
                this.lbUsernameError.Visible = true;
                isEmpty = true;
            }
            else
                this.lbUsernameError.Visible = false;

            if (password.Equals(""))
            {
                this.lbPasswordError.Text = "Password is empty";
                this.lbPasswordError.Visible = true;
                isEmpty = true;
            }
            else
                this.lbPasswordError.Visible = false;

            //če je bilo vse vnešeno, začnemo preverjati podatke
            if(!isEmpty)
            {
                /*
                string auth = userName + "|" + en.GetHash(password);// Convert.ToBase64String(Encoding.UTF8.GetBytes(auth))
                HttpClient client = new HttpClient();
                client.BaseAddress = new Uri(URLLogin);
                client.DefaultRequestHeaders.Add("Authorization",auth);

                // Add an Accept header for JSON format.
                client.DefaultRequestHeaders.Accept.Add(
                new MediaTypeWithQualityHeaderValue("application/json"));

                // List data response.
                HttpResponseMessage response = client.GetAsync("").Result;  // Blocking call!
                if (response.IsSuccessStatusCode)
                {
                    // Parse the response body. Blocking!
                    User data = response.Content.ReadAsAsync<User>().Result;
                    if (data != null)
                    {
                        this.lbPasswordError.Visible = false;
                        //če sta ime uporabnika in geslo veljavna spustimo naprej
                        Session["user"] = data;
                        Response.Redirect("Chat.aspx");
                    }
                }*/
                
                u = new User().getUserByUsername(userName);         //v seznamu poizkusim najti uporabnika, če ga ni potem vrem null

                //preverimo ali uporabnik obstaja
                if (u == null)
                {
                    this.lbUsernameError.Text = "User does not exist";
                    this.lbUsernameError.Visible = true;
                }
                //če uporabnik obstaja, šele nato preverimo ali je geslo pravilno
                else
                {
                    this.lbUsernameError.Visible = false;
                    if (!u.getPassword().Equals(en.GetHash(password)))
                    {
                        this.lbPasswordError.Text = "Wrong password";
                        this.lbPasswordError.Visible = true;
                    }
                    else
                    {
                        this.lbPasswordError.Visible = false;
                        //če sta ime uporabnika in geslo veljavna spustimo naprej
                        Session["user"] = u;
                        Response.Redirect("Chat.aspx");
                    }
                }
            }
        }

        protected void inserBurron_Click(object sender, EventArgs e) {
            //preberemo podatke iz forme
            string fname     = this.tbFirstNameIN.Text;
            string lname     = this.tbLastNameIN.Text;
            string password1 = this.tbPasswordIN.Text;
            string password2 = this.tbPasswordIN0.Text;
            string username  = this.tbUsernameIN.Text;
            bool status = true;                             //zastavica, ki nam pove če lahko uporabnika vnesemo v bazo

            //ali geslo vsebuje vsaj en poseben znak
            if (!HasSpecialCharacters(password1))
                status = error("Geslo mora vsebovati vsaj 1 poseben znak (?.*!:)");


            //ali geslo vsebuje vsaj 2 številki
            if (password1.Count(Char.IsDigit) < 2)
                status = error("Geslo mora vsebovati vsaj 2 številki");

            int i = password1.Count(Char.IsUpper);
            //ali geslo vsebuje vsaj 2 veliki črki
            if (password1.Count(Char.IsUpper) < 2)
                status = error("Geslo mora vsebovati vsaj 2 veliki črki");

            //ali se gesli ujamata
            if (!password1.Equals(password2))
                status = error("Geslo se ne ujema");

            //ali je geslo dovolj dolgo
            if (password1.Length < 8)
                status = error("Geslo mora biti dolgo vsaj 8 znakov");

            //preveri ali je bil vnešen priimek
            if (lname.Equals(""))
                status = error("Vnesi priimek");

            //preveri ali je bilo uporabniško ime
            if (username.Equals(""))
                status = error("Vnesi uporabniško ime");

            //preveri ali je bilo vnešeno ime
            if (fname.Equals(""))
                status = error("Vnesi ime uporabnika");

            //sedaj naredim uporabnika in ga vnesem v bazo
            if (status)
            {
                User u = new User(fname, lname, username, password1);
                try
                {
                    u.Persist();
                    this.lbStatus.Visible = true;
                    this.lbStatus.ForeColor = System.Drawing.Color.Green;
                    this.lbStatus.Text = "Vaš uporabniški račun je bil uspešno ustvarjen!";
                }
                catch (Exception ex)
                {
                    this.lbStatus.Visible = true;
                    this.lbStatus.ForeColor = System.Drawing.Color.Red;
                    this.lbStatus.Text = "Vaš uporabniški račun ni bil ustvarjen!";
                    Console.WriteLine(ex);
                }
            }
        }

        protected bool error(string message) {
            this.lbStatus.Visible = true;
            this.lbStatus.ForeColor = System.Drawing.Color.Red;
            this.lbStatus.Text = message;
            return false;
        }

        protected bool HasSpecialCharacters(string s)
        {
            bool contains = false;
            foreach (char c in s) {
                if (c == '?')
                {
                    contains = true;
                    break;
                }
                if (c == '!')
                {
                    contains = true;
                    break;
                }
                if (c == '.')
                {
                    contains = true;
                    break;
                }
                if (c == ':')
                {
                    contains = true;
                    break;
                }
                if (c == '*')
                {
                    contains = true;
                    break;
                }
            }
            return contains;
        }

        protected void btnAdminLogin_Click(object sender, EventArgs e)
        {
            //TODO: naredi valicadijo če je uporabnik res administrator
            Encrtypt en = new Encrtypt();
            string adminName = this.Username.Text;
            string password = this.Password.Text;
            u = new User().getUserByUsername(adminName);         //v seznamu poizkusim najti uporabnika, če ga ni potem vrem null

            if (u == null)
            {
                this.lbStatus.Text = "User does not exist";
                this.lbUsernameError.Visible = true;
            }
            //če uporabnik obstaja, šele nato preverimo ali je geslo pravilno
            else
            {
                this.lbUsernameError.Visible = false;
                if (!u.getPassword().Equals(en.GetHash(password)))
                {
                    this.lbStatus.Text = "Wrong password";
                    this.lbStatus.Visible = true;
                }
                else if (!u.getAdmin())
                {
                    this.lbStatus.Text = "You are not admin";
                    this.lbStatus.Visible = true;
                }
                else
                {
                    this.lbStatus.Visible = false;
                    //če sta ime uporabnika in geslo veljavna spustimo naprej
                    Session["user"] = u;
                    Response.Redirect("Admin.aspx");
                }
            }
        }
    }
}