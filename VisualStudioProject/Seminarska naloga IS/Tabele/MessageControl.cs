﻿using Seminarska_naloga_IS.DataAccess;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace Seminarska_naloga_IS.Tabele
{
    public static class MessageControl
    {
        private static List<string> message = new List<string>();

        public static List<string> getMessage() {
            return MessageAccess.getMessage();
        }

        public static bool setMessage(string user, string m)
        {
            return MessageAccess.setMessage(user, m);
        }
    }
}