﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Security.Cryptography;
using System.Text;

namespace Seminarska_naloga_IS.Tabele
{
    public class Encrtypt
    {
        public Encrtypt()
        { }

        public string GetHash(string input)
        {
            MD5 md5 = MD5.Create();
            byte[] data = md5.ComputeHash(Encoding.UTF8.GetBytes(input));
            String temp = "";
            for (int i = 0; i < data.Length; i++)
            {
                temp += data[i].ToString("X2");
            }
            return temp;
        }
    }
}