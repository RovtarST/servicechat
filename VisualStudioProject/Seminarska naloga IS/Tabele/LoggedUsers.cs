﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Seminarska_naloga_IS.Tabele
{
    public static class LoggedUsers //ta razred je statičen, ker ne smem narediti nove instance za vsak klic ("globalen")
    {
        private static List<User> loggedUsers = new List<User>();

        //da lahko dostopam do loggedUsers
        public static void Add(User u)
        {
            loggedUsers.Add(u);
        }

        //da lahko dostopam do loggedUsers
        public static void deleteByUsername(string u)
        {
            User u_temp = loggedUsers.Find(x => x.getUserName() == u);
            if (u_temp != null)
                loggedUsers.Remove(u_temp);
        }

        public static List<User> getUserList()
        {
            //testno populiram list
            /*loggedUsers.Add(new User("Slavoj", "Žižek", "admin", "admin"));
            loggedUsers.Add(new User("Murko", "Damjanovič", "murkod", "programer"));
            loggedUsers.Add(new User("Blaž", "Rogelj", "63150247", "student"));*/

            return loggedUsers;
        }

    }
}