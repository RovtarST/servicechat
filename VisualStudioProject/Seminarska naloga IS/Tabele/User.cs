﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.IO;
using System.Reflection;
using Seminarska_naloga_IS.DataAccess;

namespace Seminarska_naloga_IS.Tabele
{
    public class User
    {
        //lastnosti vsakega uporabnika
        public string firstName { get; set; }
        public string lastName { get; set; }
        public string userName { get; set; }
        public string password { get; set; }
        public int numberOfMessages { get; set; }
        public bool admin { get; set; }
        private string SOURCE = "Data Source = chatdbbs.database.windows.net; Initial Catalog = CHATDB; Persist Security Info=True;User ID = blazsimon; Password=Hiho1234";

        //string path = "C:\\Users\\Simon Tusar\\Source\\Repos\\chatdb\\Seminarska naloga IS\\App_Data\\chatdb.mdf";
        //to sm dal sm ka se rab v večih tizga tadruzga pa nism mogu tko da bi lohk deklarirala kt spremenljivko pa že kr v konstruktorju poklicala 
        //da ne bi blo treba not v vsak funkci res je da sta sm dve sam bi boli zgledal pomojem
        //private string path = "C:\\Users\\Simon Tusar\\Source\\Repos\\chatdb\\Seminarska naloga IS\\App_Data\\chatdb.mdf"; //sori šele pol sm vidu
        //private string path = "C:\\Users\\blazr\\Documents\\Šola\\Faks\\2. Letnik\\Informacijski sistemi\\hihane\\Seminarska naloga IS\\App_Data\\chatdb.mdf";
        //static string path = "|DataDirectory|\\chatdb.mdf";

        public User()
        {
        }

        //to mam narjen da dam notr username pa password si da pol lohk pogledam ker je uporabnik 
        //lohk bi pa nardila statične te metode za gledanje iz baze pa vpisvanje kar bi pomenil da tega ne bi blo treba 
        public User(string userName, string password)
        {
            this.userName = userName;
            this.password = password;
        }

        public User(string firstName, string lastName, string userName, string password)
        {
            this.firstName = firstName;
            this.lastName = lastName;
            this.userName = userName;
            this.password = password;
        }

        //zapišemo/posodobimo uporabnika v bazo (posodabljanje trenutno ni implementirano, ker ga ne potrebujemo)
        public void Persist()
        {
            // definiri svojo pot
            Encrtypt md5 = new Encrtypt();

            //tle je blo če ne bi delal
            //string source = "Data Source = (LocalDB)\\MSSQLLocalDB;AttachDbFilename=" + path + ";Integrated Security = True";

            using (SqlConnection connection = new SqlConnection(SOURCE))
            {
                connection.Open();
                using (SqlCommand command = new SqlCommand("INSERT INTO Uporabnik (ime,priimek,username, geslo) VALUES (@ime,@priimek,@username,@geslo)", connection))
                {
                    command.Parameters.Add(new SqlParameter("ime", this.firstName));
                    command.Parameters.Add(new SqlParameter("priimek", this.lastName));
                    command.Parameters.Add(new SqlParameter("username", this.userName));
                    command.Parameters.Add(new SqlParameter("geslo", md5.GetHash(this.password)));
                    command.ExecuteNonQuery();
                }
                connection.Close();
            }
        }

        //brisanje uporabnika in njegovih sporočil
        //TODO: treba je pobrisati še njegova sporočila
        public void Delete()
        {
            //string source = "Data Source = (LocalDB)\\MSSQLLocalDB;AttachDbFilename=" + path + ";Integrated Security = True";
            using (SqlConnection connection = new SqlConnection(SOURCE))
            {
                connection.Open();
                using (SqlCommand command = new SqlCommand("DELETE FROM Pogovor WHERE userName = @userName", connection))
                {
                    command.Parameters.Add(new SqlParameter("userName", this.userName));
                    command.ExecuteNonQuery();
                }
                connection.Close();

                connection.Open();
                using (SqlCommand command = new SqlCommand("DELETE FROM Uporabnik WHERE userName = @userName", connection))
                {
                    command.Parameters.Add(new SqlParameter("userName", this.userName));
                    command.ExecuteNonQuery();
                }
                connection.Close();
            }
        }

        public void Update()
        {
            //string source = "Data Source = (LocalDB)\\MSSQLLocalDB;AttachDbFilename=" + path + ";Integrated Security = True";
            using (SqlConnection connection = new SqlConnection(SOURCE))
            {
                connection.Open();
                using (SqlCommand command = new SqlCommand("UPDATE Uporabnik SET userName = @userName, priimek = @priimek, ime = @ime, geslo = @geslo, admin = @admin WHERE userName = @userName", connection))
                {
                    command.Parameters.Add(new SqlParameter("userName", userName));
                    command.Parameters.Add(new SqlParameter("priimek", this.lastName));
                    command.Parameters.Add(new SqlParameter("ime", this.firstName));
                    command.Parameters.Add(new SqlParameter("geslo", this.password));
                    command.Parameters.Add(new SqlParameter("admin", this.admin));
                    command.ExecuteNonQuery();
                }
                connection.Close();
            }
        }

        public string getUserName()
        {
            return this.userName;
        }

        public string getPassword()
        {
            return this.password;
        }

        public string getFirstName()
        {
            return this.firstName;
        }

        public string getLastName()
        {
            return this.lastName;
        }

        public int getNumberOfMessages() {
            return numberOfMessages;
        }
        public bool getAdmin() {
            return this.admin;
        }

        public override string ToString()
        {
            return this.firstName + " " + this.lastName;
        }

        public User getUserByUsername(string userName)
        {
            User u = new User();
            List<User> userList = new User().userList();
            u = userList.Find(x => x.userName == userName);
            return u;
        }

        public void setFirstName(string fname) {
            this.firstName = fname;
        }

        public void setLastName(string lname) {
            this.lastName = lname;
        }

        public void setPassword(string password) {
            this.password = password;
        }

        public void setUserName(string userName)
        {
            this.userName = userName;
        }

        public void setNumberOfMessages(int numberOfMessages) {
            this.numberOfMessages = numberOfMessages;
        }

        public void setAdmin(string admin) {
            if (admin.Equals("True"))
                this.admin = true;
            else
                this.admin = false;
        }

        public List<User> userList()
        {
            return UserAccess.getUsers();
         
            //že vkodiram uporabnike
            /*List<User> userList = new List<User>();
            userList.Add(new User("Slavoj", "Žižek", "admin", "Geslo.Admin"));
            userList.Add(new User("Murko", "Damjanovič", "murkod", "Geslo.Programer"));
            userList.Add(new User("Blaž", "Rogelj", "63150247", "Geslo.Student"));
            return userList;*/
        }

    }

}
