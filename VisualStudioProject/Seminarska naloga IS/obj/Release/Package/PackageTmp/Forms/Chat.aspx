﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Chat.aspx.cs" Inherits="Seminarska_naloga_IS.Forms.Chat" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="../CSS/Chat.css" rel="stylesheet" type="text/css"/>
</head>
<body>
    <form id="form1" runat="server">
            <div id="upLeft">
                <asp:Label ID="Label1" runat="server" Text="Pozdravljeni prijavljeni ste kot: "></asp:Label>
                <asp:Label ID="CurrentUser" runat="server" Text="lbUser"></asp:Label>
                <asp:HiddenField ID="hiddenUsername" runat="server" />
            </div>
            <div id="upRight">
                <asp:Button ID="Logout" runat="server" Text="Odjava" CssClass="button" OnClick="LogoutBtn_Click"/>
            </div>

            <div id="midLeft">
                <asp:PlaceHolder ID="Messages" runat="server"></asp:PlaceHolder>
            </div>

            <div id="midRightUp">
                <asp:PlaceHolder ID="Users" runat="server"></asp:PlaceHolder>
            </div>

            <div id ="midRightDown">
                <asp:Button ID="Refresh" runat="server" Text="Osveži" CssClass="button" OnClick="RefreshBtn_Clicked"/>
            </div>
        
            <div id="downLeft">
                <asp:TextBox ID="Message" runat="server" CssClass="textbox"></asp:TextBox>
            </div>

            <div id="downRight">
                <asp:Button ID="Send" runat="server" Text="Pošlji" CssClass="button" OnClick="SendBtn_Clicked"/>
            </div>  
    <div style="clear:both"></div>

    </form>
</body>
</html>
