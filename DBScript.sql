CREATE TABLE [dbo].[Uporabnik] (
    [username] VARCHAR (30) NOT NULL,
    [ime]      VARCHAR (30) NULL,
    [priimek]  VARCHAR (30) NULL,
    [geslo]    CHAR (32)    NULL,
    [Admin]    BIT          NULL,
    PRIMARY KEY CLUSTERED ([username] ASC)
);

CREATE TABLE [dbo].[Pogovor] (
    [Id]       INT                IDENTITY (1, 1) NOT NULL,
    [username] VARCHAR (30)       NULL,
    [besedilo] VARCHAR (512)      NULL,
    [Date]     DATETIMEOFFSET (7) DEFAULT ((CONVERT([datetimeoffset],sysdatetimeoffset()) AT TIME ZONE 'Central European Standard Time')) NULL,
    PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [username] FOREIGN KEY ([username]) REFERENCES [dbo].[Uporabnik] ([username])
);