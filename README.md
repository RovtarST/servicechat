# ChatDB #

##Avtorji:##

* Blaž Rogelj 63150247
* Simon Tušar 63150302

### Uporabniki ###

|Uporabniško ime|  Ime   |  Priimek   |   Geslo  |
|---------------|--------|------------|----------|
| Student.Blaz  | Blaž   | Rogelj     | 12345.BR |
| Student.Simon | Simon  | Tusar      | 12345.ST |
| Admin         | Slavoj | Žužek      | 12345.SZ |
| Murkod        | Murko  | Damjanovič | 12345.MD |

Administratorja sta uporabnika Student.Blaz in Student.Simon.

Povezava na klepetalnico: http://seminarskaisbs.azurewebsites.net/Forms/Login.aspx

### Slike uporabniškega vmesnika ###

![LoginS2.PNG](https://bitbucket.org/repo/adGobR/images/2747415207-LoginS2.PNG)
![ChatIS2.PNG](https://bitbucket.org/repo/adGobR/images/848909736-ChatIS2.PNG)
![ADMIN CONZOLE.png](https://bitbucket.org/repo/adGobR/images/1475944292-ADMIN%20CONZOLE.png)
![Screenshot_20170111-183612[1].png](https://bitbucket.org/repo/adGobR/images/3032018079-Screenshot_20170111-183612%5B1%5D.png)
![Screenshot_20170111-183619[1].png](https://bitbucket.org/repo/adGobR/images/642591328-Screenshot_20170111-183619%5B1%5D.png)
![Screenshot_20170111-183624[1].png](https://bitbucket.org/repo/adGobR/images/697380509-Screenshot_20170111-183624%5B1%5D.png)

![baza1.png](https://bitbucket.org/repo/adGobR/images/3504121650-baza1.png)
SQL poizvedba, ki ustvari tabelo Uporabnik
![baza 2.png](https://bitbucket.org/repo/adGobR/images/1378190215-baza%202.png)
SQL poizvedba, ki ustvari tabelo Pogovor in samodejno posodablja čas ob poslanem sporočilu.
![Screenshot_1.png](https://bitbucket.org/repo/adGobR/images/764887311-Screenshot_1.png)
Shema baze

###Opis###

Delovanje aplikacije je podobno prejšnji verziji, pošilja sporočila, omogoča uporabnikov vpis. Dodano pa je da se uporabnik lahko registrira, tej podatki se nato zapišejo v podatkovno bazo, pri pošiljanju sporočil se sporočila hranijo v podatkovno bazo, prav tako pa z pomočjo baze preverjamo ali je uporabnik vpisal pravilno uporabniško ime in geslo.

Pri izdelavi te seminarske naloge nama je največ težav povzročal git, saj ga nisva bila navajena. Prav tako, sva imela probleme z kodo, saj vsaka od naju piše na malo drugačen način in sva se morala na koncu vsak malo prilagoditi. Edina izmed težav, ki nama jo je povzročila izdelava projekta je bila ta, da nisva vedela kako uporabiti relativno pot do podatkovne baze.

UPDATE:
V administratorski konzoli imamo seznam registriranih uporabnikov. Če imamo status administratorja lahko uporabnika urejamo tako, da kliknemo na uporabnika, da se ta obarva z modro barvo in kliknemo gumb Uredi. Izbranega uporabnika lahko izbrišemo ali pa mu bodisi dodamo bodisi odvzamemo administratorsko pravico. Spremembo uveljavimo
tako, da kliknemo gumb Shrani.

Največja težava je bila povezava in registracija na portalu Azure, saj so bila zastarela navodila in linki, kako ustvariti nov študentski račun.

Izboljšave:

* Lepši user interface
* Ob pritisku na gumb Enter bi lahko poslal sporočilo (izboljšava je sedaj narejena)
* Lepša administratorska konzola

###Opis nalog###
Simon Tušar

* Kriptiranje gesla: Encrypt
* Narejena baza
* Narejena relativna pot: MessageAccess, UserAcces, User
* Popravljanje baze pred oddajo
* Napisani RESTi
* Povezava z Azurom
* Narejena android aplikacija

Blaž Rogelj

* Izgled: CSS 
* Branje in vnos v bazo: MessageAccess, UserAcces, User
* Registracija uporabnika: Login
* Narejena administratorska konzola
* Napisane SQL poizvedbe