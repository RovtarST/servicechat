package com.example.blazrogelj.androidis;

/**
 * Created by simon on 3. 01. 2017.
 */

public class Messages {
    private int ID;
    private String text;
    private String userName;

    public Messages(int ID, String Text, String Username)
    {
        this.ID = ID;
        this.text = Text;
        this.userName = Username;
    }

    public void setID(int ID){ this.ID = ID; }
    public void setText(String text){ this.text = text; }
    public void setUserName(String userName){ this.userName = userName; }

    public int getID() { return ID; }
    public String getText() { return text; }
    public String getUserName() { return userName; }

    @Override
    public String toString(){
        return this.userName + " : " + this.text;
    }
}
