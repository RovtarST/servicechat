package com.example.blazrogelj.androidis;

import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class ChatActivity extends AppCompatActivity {
    TextView tvMessages;
    String username = null;
    String password = null;
    String tempMessage;
    String messages="";
    int lastMessage = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);

        Button btnRefresh   = (Button)findViewById(R.id.btnRefresh);
        Button btnSend      = (Button)findViewById(R.id.btnSend);
        final EditText etMessage  = (EditText)findViewById(R.id.etMessage);
        tvMessages = (TextView)findViewById(R.id.tvMessages);

        btnRefresh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                callREST();
            }
        });

        btnSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                tempMessage = etMessage.getText().toString();
                if(!callRESTSend())
                {
                    Toast.makeText(ChatActivity.this, "ERROR Message was not send", Toast.LENGTH_LONG).show();
                }
                else
                {
                    Toast.makeText(ChatActivity.this, "Sporocilo poslano uspesno", Toast.LENGTH_LONG).show();
                    callREST();
                    etMessage.setText("");
                }
            }
        });

        Intent intet = getIntent();
        Bundle bundle = intet.getExtras();
        username = bundle.getString("com.example.blazrogelj.androidis.user");
        password= bundle.getString("com.example.blazrogelj.androidis.password");
        if(username != null)
        {
            callREST();
        }
    }

    public void callREST()
    {
        //orderActivity.RESTCallTask restTask = new orderActivity.RESTCallTask();
        List<Messages> result = new ArrayList<Messages>();
        try
        {
            result = new ChatActivity.RESTCallTask().execute().get();
        }
        catch (Exception e)
        {

        }
        for(Messages temp : result)
        {
            messages+= temp.toString() +"\n";
            lastMessage = temp.getID();
        }
        tvMessages.setText(messages);
    }

    private class RESTCallTask extends AsyncTask<String, Void, List<Messages>> {

        @Override
        protected List<Messages> doInBackground(String... params) {
            String URLString = "http://seminarskaisbs.azurewebsites.net/Service1.svc/Messages";

            List<Messages> result = new ArrayList<Messages>();
            try {
                StringBuffer resultT = new StringBuffer("");
                URL url = new URL(URLString);
                HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                String authorization = username + "|" + password;
                connection.setRequestProperty("Authorization", authorization);
                connection.setRequestProperty("Index", Integer.toString(lastMessage));
                //connection.setRequestMethod("POST");
                //connection.setDoInput(true);
                connection.connect();


                InputStream inputStream = connection.getInputStream();

                BufferedReader rd = new BufferedReader(new InputStreamReader(inputStream));
                String line = "";

                while ((line = rd.readLine()) != null) {
                    resultT.append(line);
                }

                JSONArray array = new JSONArray(resultT.toString());
                for (int i = 0; i < array.length(); i++) {
                    JSONObject jsonObject = array.getJSONObject(i);
                    result.add(new Messages(
                            jsonObject.getInt("ID"),
                            jsonObject.getString("Text"),
                            jsonObject.getString("Username")
                    ));
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return result;
        }
    }

    public boolean callRESTSend() {
        try {
            return new ChatActivity.RESTSendCallTask().execute().get();
        }
        catch (Exception e)
        {}
        return false;
    }

    private class RESTSendCallTask extends AsyncTask<String, Void, Boolean> {
        private final String SERVICE_URL = "http://seminarskaisbs.azurewebsites.net/Service1.svc/Send";

        @Override
        protected Boolean doInBackground(String... params) {
            String urlString = SERVICE_URL;

            boolean result = false;
            try {

                URL url = new URL(urlString);
                HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                String authorization = username + "|" + password;
                connection.setRequestProperty("Authorization", authorization);
                connection.setRequestProperty("SporociloText", tempMessage);
                //connection.setRequestMethod("POST");
                //connection.setDoInput(true);
                connection.connect();

                InputStream inputStream = connection.getInputStream();
                BufferedReader rd = new BufferedReader(new InputStreamReader(inputStream));
                String line = "";

                while ((line = rd.readLine()) != null) {
                    if (line.equals("true"))
                        result = true;
                    else
                        result = false;
                }
            } catch (Exception e) {
                // Writing exception to log
                e.printStackTrace();
            }

            return result;
        }
    }
}
