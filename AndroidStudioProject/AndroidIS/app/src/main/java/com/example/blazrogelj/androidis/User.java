package com.example.blazrogelj.androidis;

/**
 * Created by Blaz Rogelj on 31. 12. 2016.
 */

public class User {
    private String userName;
    private String firstName;
    private String lastName;
    private String password;
    private String numberOfMessages;
    private boolean admin;

    public User(String userName, String firstName, String lastName, String password, String numberOfMessages, boolean admin) {
        this.userName  = userName;
        this.firstName = firstName;
        this.lastName  = lastName;
        this.password  = password;
        this.numberOfMessages = numberOfMessages;
        this.admin = admin;
    }

    public User() {

    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastname() {
        return lastName;
    }

    public void setLastname(String lastName) {
        this.lastName = lastName;
    }

    public String getPassword(){ return  password; }

    public void setPassword(String password) { this.password = password; }

    public String getNumberOfMessages(){ return  numberOfMessages; }

    public void setNumberOfMessages(String numberOfMessages) { this.numberOfMessages = numberOfMessages; }

    public boolean getAdmin(){ return  admin; }

    public void setAdmin(boolean admin) { this.admin = admin; }

    @Override
    public String toString() {
        return "User{" +
                "userName=" + userName +
                ", firstName='" + firstName + '\'' +
                ", lastname='" + lastName + '\'' +
                ", password='" + password + '\'' +
                ", admin='" + admin + '\'' +
                ", numberOfMessages='" + numberOfMessages + '\'' +
                '}';
    }
}
