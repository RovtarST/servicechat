package com.example.blazrogelj.androidis;


import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import java.security.MessageDigest;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.UnknownHostException;
import java.security.NoSuchAlgorithmException;

public class MainActivity extends AppCompatActivity {

    User user;
    String username;
    String password;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button btnLogin = (Button)findViewById(R.id.btnLogin);
        final EditText etUser = (EditText)findViewById(R.id.etUsername);
        final EditText etPassword = (EditText) findViewById(R.id.etPassword);

        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                user = null;
                username = etUser.getText().toString();
                password = etPassword.getText().toString();

                MessageDigest md = null;
                try {
                    md = MessageDigest.getInstance("MD5");
                } catch (NoSuchAlgorithmException e) {
                    e.printStackTrace();
                }
                md.update(password.getBytes());

                byte byteData[] = md.digest();
                StringBuffer hexString = new StringBuffer();
                for (int i=0;i<byteData.length;i++) {
                    String hex=Integer.toHexString(0xFF & byteData[i]);
                    if(hex.length()==1) hexString.append('0');
                    hexString.append(hex);
                }
                password = hexString.toString().toUpperCase();

                callREST();
                //Intent intent = new Intent(MainActivity.this, ChatActivity.class)
                if (user != null) {
                    Intent mainIntent = new Intent(MainActivity.this, ChatActivity.class);
                    Bundle bundle = new Bundle();
                    bundle.putString("com.example.blazrogelj.androidis.user", user.getUserName());
                    bundle.putString("com.example.blazrogelj.androidis.password", user.getPassword());
                    mainIntent.putExtras(bundle);
                    startActivity(mainIntent);
                }
                else
                {
                    Toast.makeText(MainActivity.this, "Wrong username or password!", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    /**
     * Represents an asynchronous login/registration task used to authenticate
     * the user.
     */


    //TUKAJ SEM ODAL REST
    public void callREST() {
        try {
            //user = new MainActivity.RESTCallTask().execute().get();
            user = new MainActivity.RESTCallTask().execute().get();
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }


    private class RESTCallTask extends AsyncTask<String, Void, User> {
        private final String SERVICE_URL = "http://seminarskaisbs.azurewebsites.net/Service1.svc/Login";

        @Override
        protected User doInBackground(String... params) {
            String urlString = SERVICE_URL;
            User result = null;

            try {
                StringBuffer resultT = new StringBuffer("");
                URL url = new URL(urlString);
                HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                String authorization = username + "|" + password;
                connection.setRequestProperty("Authorization", authorization);
                connection.connect();

                InputStream inputStream = connection.getInputStream();
                BufferedReader rd = new BufferedReader(new InputStreamReader(inputStream));
                String line = "";

                while ((line = rd.readLine()) != null) {
                    resultT.append(line);
                }

                JSONObject jsonObject = new JSONObject(resultT.toString());
                result = new User(
                        jsonObject.getString("userName"),
                        jsonObject.getString("firstName"),
                        jsonObject.getString("lastName"),
                        jsonObject.getString("password"),
                        jsonObject.getString("numberOfMessages"),
                        jsonObject.getBoolean("admin")
                );
            }
            catch (UnknownHostException e)
            {
                //Toast.makeText(MainActivity.this, "No internet connection!", Toast.LENGTH_SHORT).show();
            }
            catch (Exception e) {
                e.printStackTrace();
            }
            return result;
        }
    }
}
